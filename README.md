# Mikrotik IPsec configuration role.

It allows to configure IPsec on Mikrotik RouterOS.

Example usage is in docs/

Be careful: the role deletes all your old configuration.


Warning: the role is intended to use with a new format of IPsec configuration (start from version 6.44beta61).
The role will not work with older RouterOS versions.
